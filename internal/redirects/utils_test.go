package redirects

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestIsDomainURL(t *testing.T) {
	tests := map[string]struct {
		url          string
		expectedBool bool
	}{
		"only_path": {
			url:          "/goto.html",
			expectedBool: false,
		},
		"valid_domain_url": {
			url:          "https://GitLab.com",
			expectedBool: true,
		},
		"schemaless_domain_url_with_special_char": {
			url:          "/\\GitLab.com",
			expectedBool: false,
		},
		"schemaless_domain_url": {
			url:          "//GitLab.com/pages.html",
			expectedBool: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			require.EqualValues(t, tt.expectedBool, isDomainURL(tt.url))
		})
	}
}

func TestMatchHost(t *testing.T) {
	tests := map[string]struct {
		originalURL  string
		path         string
		expectedBool bool
		expectedPath string
	}{
		"path_without_domain": {
			originalURL:  "https://GitLab.com/goto.html",
			path:         "/goto.html",
			expectedBool: true,
			expectedPath: "/goto.html",
		},
		"valid_matching_host": {
			originalURL:  "https://GitLab.com/goto.html",
			path:         "https://GitLab.com/goto.html",
			expectedBool: true,
			expectedPath: "/goto.html",
		},
		"different_schema_path": {
			originalURL:  "http://GitLab.com/goto.html",
			path:         "https://GitLab.com/goto.html",
			expectedBool: false,
			expectedPath: "",
		},
		"different_host_path": {
			originalURL:  "https://GitLab.com/goto.html",
			path:         "https://GitLab-test.com/goto.html",
			expectedBool: false,
			expectedPath: "",
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			parsedURL, err := url.Parse(tt.originalURL)
			require.NoError(t, err)

			hostMatches, path := matchHost(parsedURL, tt.path)
			require.EqualValues(t, tt.expectedBool, hostMatches)
			require.EqualValues(t, tt.expectedPath, path)
		})
	}
}
