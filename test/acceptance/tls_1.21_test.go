//go:build go1.21

package acceptance_test

const tlsMissingCertificateErrorMessage = "tls: certificate required"
